#!python3

"""

"""

# METADATA
__author__ = "Rinze-Pieter Jonker"
__version__ = "1.0"

# IMPORTS
import ac, acsys
try:
	import cue_sdk
	from cue_sdk import *
except ImportError as e:
	pass


class KeyBoardHandler:
	def __init__(self, mode = "Center", idle = 0, maximum = 0):
		self.modes = ["Center", "LeftToRight"]
		self.mode = mode
		
		self.corsair = self.enable_corsair()
		self.lights_on, self.lights_off, self.lights_modifier = self.set_lights()

		self.last_rpms = [0]
		self.max_rpm = round(maximum)
		if self.max_rpm != 0:
			self.change_max = False
		else:
			self.change_max	= True
		self.min_rpm = self.max_rpm * 0.8

		self.battery_index = 0
		self.battery_use_index = 0
		self.same = 0
		self.idle = idle
		self.delta = 0
		self.deltas = [0.0] * 1000
		self.since_blink = 0

		self.change = False
		self.blink = False

		self.exceptions = ""
	
	def enable_corsair(self):
		"""
		This functions turns on the corsair SDK so that it can be used to handle the keyboard side of the code
		""" 
		corsair = CUESDK("G:/SteamLibrary/steamapps/common/assettocorsa/apps/python/CorsairKeyboardRPMs/CUESDK.x64_2013.dll")
		corsair.request_control(CAM.ExclusiveLightingControl)
		return corsair

	def set_lights(self):
		"""
		This function handles the mode selection of the lights and the multiplier used to go through the
		"""
		if self.mode == "Center":
			# moving to center
			lights_modifier = 2

			lights_on = [
				[CorsairLedColor(CLK.GraveAccentAndTilde, 0, 255, 0),
				CorsairLedColor(CLK.Backspace, 0, 255, 0),
				CorsairLedColor(CLK._1, 0, 255, 0),
				CorsairLedColor(CLK._2, 0, 255, 0),
				CorsairLedColor(CLK.EqualsAndPlus, 255, 255, 0),
				CorsairLedColor(CLK._3, 255, 255, 0),
				CorsairLedColor(CLK.MinusAndUnderscore, 255, 255, 0),
				CorsairLedColor(CLK._4, 255, 255, 0),
				CorsairLedColor(CLK._0, 255, 255, 0),
				CorsairLedColor(CLK._5, 255, 0, 0),
				CorsairLedColor(CLK._9, 255, 0, 0),
				CorsairLedColor(CLK._6, 255, 0, 0),
				CorsairLedColor(CLK._8, 255, 0, 0),
				CorsairLedColor(CLK._7, 255, 0, 0)],
				[CorsairLedColor(CLK.Tab, 0, 255, 0),
				CorsairLedColor(CLK.BracketRight, 0, 255, 0),
				CorsairLedColor(CLK.Q, 0, 255, 0),
				CorsairLedColor(CLK.BracketLeft, 0, 255, 0),
				CorsairLedColor(CLK.W, 255, 255, 0),
				CorsairLedColor(CLK.P, 255, 255, 0),
				CorsairLedColor(CLK.E, 255, 255, 0),
				CorsairLedColor(CLK.O, 255, 255, 0),
				CorsairLedColor(CLK.R, 255, 0, 0),
				CorsairLedColor(CLK.I, 255, 0, 0),
				CorsairLedColor(CLK.T, 255, 0, 0),
				CorsairLedColor(CLK.U, 255, 0, 0),
				CorsairLedColor(CLK.Y, 255, 0, 0)],
				[CorsairLedColor(CLK.CapsLock, 0, 255, 0),
				CorsairLedColor(CLK.Enter, 0, 255, 0),
				CorsairLedColor(CLK.A, 0, 255, 0),
				CorsairLedColor(CLK.ApostropheAndDoubleQuote, 0, 255, 0),
				CorsairLedColor(CLK.S, 255, 255, 0),
				CorsairLedColor(CLK.SemicolonAndColon, 255, 255, 0),
				CorsairLedColor(CLK.D, 255, 255, 0),
				CorsairLedColor(CLK.L, 255, 255, 0),
				CorsairLedColor(CLK.F, 255, 0, 0),
				CorsairLedColor(CLK.K, 255, 0, 0),
				CorsairLedColor(CLK.G, 255, 0, 0),
				CorsairLedColor(CLK.J, 255, 0, 0),
				CorsairLedColor(CLK.H, 255, 0, 0)],
				[CorsairLedColor(CLK.LeftShift, 0, 255, 0),
				CorsairLedColor(CLK.RightShift, 0, 255, 0),
				CorsairLedColor(CLK.Z, 0, 255, 0),
				CorsairLedColor(CLK.SlashAndQuestionMark, 0, 255, 0),
				CorsairLedColor(CLK.X, 255, 255, 0),
				CorsairLedColor(CLK.PeriodAndBiggerThan, 255, 255, 0),
				CorsairLedColor(CLK.C, 255, 255, 0),
				CorsairLedColor(CLK.CommaAndLessThan, 255, 0, 0),
				CorsairLedColor(CLK.V, 255, 0, 0),
				CorsairLedColor(CLK.M, 255, 0, 0),
				CorsairLedColor(CLK.B, 255, 0, 0),
				CorsairLedColor(CLK.N, 255, 0, 0)]]

			lights_off = [
				[CorsairLedColor(CLK.GraveAccentAndTilde, 0, 0, 0),
				CorsairLedColor(CLK.Backspace, 0, 0, 0),
				CorsairLedColor(CLK._1, 0, 0, 0),
				CorsairLedColor(CLK._2, 0, 0, 0),
				CorsairLedColor(CLK.EqualsAndPlus, 0, 0, 0),
				CorsairLedColor(CLK._3, 0, 0, 0),
				CorsairLedColor(CLK.MinusAndUnderscore, 0, 0, 0),
				CorsairLedColor(CLK._4, 0, 0, 0),
				CorsairLedColor(CLK._0, 0, 0, 0),
				CorsairLedColor(CLK._5, 0, 0, 0),
				CorsairLedColor(CLK._9, 0, 0, 0),
				CorsairLedColor(CLK._6, 0, 0, 0),
				CorsairLedColor(CLK._8, 0, 0, 0),
				CorsairLedColor(CLK._7, 0, 0, 0)],
				[CorsairLedColor(CLK.Tab, 0, 0, 0),
				CorsairLedColor(CLK.BracketRight, 0, 0, 0),
				CorsairLedColor(CLK.Q, 0, 0, 0),
				CorsairLedColor(CLK.BracketLeft, 0, 0, 0),
				CorsairLedColor(CLK.W, 0, 0, 0),
				CorsairLedColor(CLK.P, 0, 0, 0),
				CorsairLedColor(CLK.E, 0, 0, 0),
				CorsairLedColor(CLK.O, 0, 0, 0),
				CorsairLedColor(CLK.R, 0, 0, 0),
				CorsairLedColor(CLK.I, 0, 0, 0),
				CorsairLedColor(CLK.T, 0, 0, 0),
				CorsairLedColor(CLK.U, 0, 0, 0),
				CorsairLedColor(CLK.Y, 0, 0, 0)],
				[CorsairLedColor(CLK.CapsLock, 0, 0, 0),
				CorsairLedColor(CLK.Enter, 0, 0, 0),
				CorsairLedColor(CLK.A, 0, 0, 0),
				CorsairLedColor(CLK.ApostropheAndDoubleQuote, 0, 0, 0),
				CorsairLedColor(CLK.S, 0, 0, 0),
				CorsairLedColor(CLK.SemicolonAndColon, 0, 0, 0),
				CorsairLedColor(CLK.D, 0, 0, 0),
				CorsairLedColor(CLK.L, 0, 0, 0),
				CorsairLedColor(CLK.F, 0, 0, 0),
				CorsairLedColor(CLK.K, 0, 0, 0),
				CorsairLedColor(CLK.G, 0, 0, 0),
				CorsairLedColor(CLK.J, 0, 0, 0),
				CorsairLedColor(CLK.H, 0, 0, 0)],
				[CorsairLedColor(CLK.LeftShift, 0, 0, 0),
				CorsairLedColor(CLK.RightShift, 0, 0, 0),
				CorsairLedColor(CLK.Z, 0, 0, 0),
				CorsairLedColor(CLK.SlashAndQuestionMark, 0, 0, 0),
				CorsairLedColor(CLK.X, 0, 0, 0),
				CorsairLedColor(CLK.C, 0, 0, 0),
				CorsairLedColor(CLK.PeriodAndBiggerThan, 0, 0, 0),
				CorsairLedColor(CLK.V, 0, 0, 0),
				CorsairLedColor(CLK.CommaAndLessThan, 0, 0, 0),
				CorsairLedColor(CLK.B, 0, 0, 0),
				CorsairLedColor(CLK.M, 0, 0, 0),
				CorsairLedColor(CLK.N, 0, 0, 0)]]
		else:
			lights_modifier = 1

			lights_on = [
			[CorsairLedColor(CLK.GraveAccentAndTilde, 0, 255, 0),
			CorsairLedColor(CLK._1, 0, 255, 0),
			CorsairLedColor(CLK._2, 0, 255, 0),
			CorsairLedColor(CLK._3, 0, 255, 0),
			CorsairLedColor(CLK._4, 0, 255, 0),
			CorsairLedColor(CLK._5, 255, 255, 0),
			CorsairLedColor(CLK._6, 255, 255, 0),
			CorsairLedColor(CLK._7, 255, 255, 0),
			CorsairLedColor(CLK._8, 255, 255, 0),
			CorsairLedColor(CLK._9, 255, 0, 0),
			CorsairLedColor(CLK._0, 255, 0, 0),
			CorsairLedColor(CLK.MinusAndUnderscore, 255, 0, 0),
			CorsairLedColor(CLK.EqualsAndPlus, 255, 0, 0),
			CorsairLedColor(CLK.Backspace, 255, 0, 0)],
			[CorsairLedColor(CLK.Tab, 0, 255, 0),
			CorsairLedColor(CLK.Q, 0, 255, 0),
			CorsairLedColor(CLK.W, 0, 255, 0),
			CorsairLedColor(CLK.E, 0, 255, 0),
			CorsairLedColor(CLK.R, 255, 255, 0),
			CorsairLedColor(CLK.T, 255, 255, 0),
			CorsairLedColor(CLK.Y, 255, 255, 0),
			CorsairLedColor(CLK.U, 255, 255, 0),
			CorsairLedColor(CLK.I, 255, 255, 0),
			CorsairLedColor(CLK.O, 255, 0, 0),
			CorsairLedColor(CLK.P, 255, 0, 0),
			CorsairLedColor(CLK.BracketLeft, 255, 0, 0),
			CorsairLedColor(CLK.BracketRight, 2555, 0, 0)],
			[CorsairLedColor(CLK.CapsLock, 0, 255, 0),
			CorsairLedColor(CLK.A, 0, 255, 0),
			CorsairLedColor(CLK.S, 0, 255, 0),
			CorsairLedColor(CLK.D, 0, 255, 0),
			CorsairLedColor(CLK.F, 255, 255, 0),
			CorsairLedColor(CLK.G, 255, 255, 0),
			CorsairLedColor(CLK.H, 255, 255, 0),
			CorsairLedColor(CLK.J, 255, 255, 0),
			CorsairLedColor(CLK.K, 255, 255, 0),
			CorsairLedColor(CLK.L, 255, 0, 0),
			CorsairLedColor(CLK.SemicolonAndColon, 255, 0, 0),
			CorsairLedColor(CLK.ApostropheAndDoubleQuote, 255, 0, 0),
			CorsairLedColor(CLK.Enter, 255, 0, 0)],
			[CorsairLedColor(CLK.LeftShift, 0, 255, 0),
			CorsairLedColor(CLK.Z, 0, 255, 0),
			CorsairLedColor(CLK.X, 0, 255, 0),
			CorsairLedColor(CLK.C, 0, 255, 0),
			CorsairLedColor(CLK.V, 255, 255, 0),
			CorsairLedColor(CLK.B, 255, 255, 0),
			CorsairLedColor(CLK.N, 255, 255, 0),
			CorsairLedColor(CLK.M, 255, 255, 0),
			CorsairLedColor(CLK.CommaAndLessThan, 255, 0, 0),
			CorsairLedColor(CLK.PeriodAndBiggerThan, 255, 0, 0),
			CorsairLedColor(CLK.SlashAndQuestionMark, 255, 0, 0),
			CorsairLedColor(CLK.RightShift, 255, 0, 0)]
			]

			lights_off = [
			[CorsairLedColor(CLK.GraveAccentAndTilde, 0, 0, 0),
			CorsairLedColor(CLK._1, 0, 0, 0),
			CorsairLedColor(CLK._2, 0, 0, 0),
			CorsairLedColor(CLK._3, 0, 0, 0),
			CorsairLedColor(CLK._4, 0, 0, 0),
			CorsairLedColor(CLK._5, 0, 0, 0),
			CorsairLedColor(CLK._6, 0, 0, 0),
			CorsairLedColor(CLK._7, 0, 0, 0),
			CorsairLedColor(CLK._8, 0, 0, 0),
			CorsairLedColor(CLK._9, 0, 0, 0),
			CorsairLedColor(CLK._0, 0, 0, 0),
			CorsairLedColor(CLK.MinusAndUnderscore, 0, 0, 0),
			CorsairLedColor(CLK.EqualsAndPlus, 0, 0, 0),
			CorsairLedColor(CLK.Backspace, 0, 0, 0)],
			[CorsairLedColor(CLK.Tab, 0, 0, 0),
			CorsairLedColor(CLK.Q, 0, 0, 0),
			CorsairLedColor(CLK.W, 0, 0, 0),
			CorsairLedColor(CLK.E, 0, 0, 0),
			CorsairLedColor(CLK.R, 0, 0, 0),
			CorsairLedColor(CLK.T, 0, 0, 0),
			CorsairLedColor(CLK.Y, 0, 0, 0),
			CorsairLedColor(CLK.U, 0, 0, 0),
			CorsairLedColor(CLK.I, 0, 0, 0),
			CorsairLedColor(CLK.O, 0, 0, 0),
			CorsairLedColor(CLK.P, 0, 0, 0),
			CorsairLedColor(CLK.BracketLeft, 0, 0, 0),
			CorsairLedColor(CLK.BracketRight, 0, 0, 0)],
			[CorsairLedColor(CLK.CapsLock, 0, 0, 0),
			CorsairLedColor(CLK.A, 0, 0, 0),
			CorsairLedColor(CLK.S, 0, 0, 0),
			CorsairLedColor(CLK.D, 0, 0, 0),
			CorsairLedColor(CLK.F, 0, 0, 0),
			CorsairLedColor(CLK.G, 0, 0, 0),
			CorsairLedColor(CLK.H, 0, 0, 0),
			CorsairLedColor(CLK.J, 0, 0, 0),
			CorsairLedColor(CLK.K, 0, 0, 0),
			CorsairLedColor(CLK.L, 0, 0, 0),
			CorsairLedColor(CLK.SemicolonAndColon, 0, 0, 0),
			CorsairLedColor(CLK.ApostropheAndDoubleQuote, 0, 0, 0),
			CorsairLedColor(CLK.Enter, 0, 0, 0)],
			[CorsairLedColor(CLK.LeftShift, 0, 0, 0),
			CorsairLedColor(CLK.Z, 0, 0, 0),
			CorsairLedColor(CLK.X, 0, 0, 0),
			CorsairLedColor(CLK.C, 0, 0, 0),
			CorsairLedColor(CLK.V, 0, 0, 0),
			CorsairLedColor(CLK.B, 0, 0, 0),
			CorsairLedColor(CLK.N, 0, 0, 0),
			CorsairLedColor(CLK.M, 0, 0, 0),
			CorsairLedColor(CLK.CommaAndLessThan, 0, 0, 0),
			CorsairLedColor(CLK.PeriodAndBiggerThan, 0, 0, 0),
			CorsairLedColor(CLK.SlashAndQuestionMark, 0, 0, 0),
			CorsairLedColor(CLK.RightShift, 0, 0, 0)]
			]

		return lights_on, lights_off, lights_modifier

	def update_rpm_variables(self, rpm: int, low_percentage = 0.8):
		"""

		:param rpm:
		:param low_percentage:
		:return:
		"""
		self.max_rpm = max(self.last_rpms)
		self.min_rpm = self.max_rpm * low_percentage

	def update_rpm(self, rpm: int):
		"""
		This function updates the RGB lights on the the keyboard according to the car rpms

		:param rpm:
		"""
		if len(self.last_rpms) > 1000:
			self.last_rpms.pop(0)

		self.last_rpms.append(rpm)

		if self.change_max:
			self.update_rpm_variables(rpm)
		self.update_idle(rpm)

		percentage = (rpm - self.min_rpm) / (self.max_rpm - self.min_rpm)

		# Setting the percentages for the rpmlights and correcting them if there are errors
		percentage_first = round(((len(self.lights_on[0]) / self.lights_modifier) * percentage) * self.lights_modifier)
		percentage_second = round(((len(self.lights_on[1]) / self.lights_modifier) * percentage) * self.lights_modifier)
		percentage_third = round(((len(self.lights_on[2]) / self.lights_modifier) * percentage) * self.lights_modifier)
		percentage_fourth = round(((len(self.lights_on[3]) / self.lights_modifier) * percentage) * self.lights_modifier)

		# Correcting them so that there are no index errors values
		# if percentage_second == 0:
		# 	percentage_second = self.lights_modifier

		# ac.log(percentage_first)

		if percentage_first < 0:
			percentage_first = 0
		elif percentage_first > len(self.lights_on[0]):
			percentage_first = len(self.lights_on[0])
		if percentage_second < 0:
			percentage_second = 0
		elif percentage_second > len(self.lights_on[1]):
			percentage_second = len(self.lights_on[1])

		if percentage_third < 0:
			percentage_third = 0
		elif percentage_third > len(self.lights_on[2]):
			percentage_third = len(self.lights_on[2])

		if percentage_fourth < 0:
			percentage_fourth = 0
		elif percentage_fourth > len(self.lights_on[3]):
			percentage_fourth = len(self.lights_on[3])

		# Setting key colors
		try:
			self.corsair.set_led_colors_async(led_colors=self.lights_on[0][:percentage_first]  + self.lights_off[0][percentage_first:])
			self.corsair.set_led_colors_async(led_colors=self.lights_on[1][:percentage_second] + self.lights_off[1][percentage_second:])
			self.corsair.set_led_colors_async(led_colors=self.lights_on[2][:percentage_third]  + self.lights_off[2][percentage_third:])
			self.corsair.set_led_colors_async(led_colors=self.lights_on[3][:percentage_fourth] + self.lights_off[3][percentage_fourth:])
		except:
			# Quick fix, still trying to find the right error
			pass

	def update_idle(self, rpm: int):
		"""

		:param rpm:
		:return:
		"""
		try:
			if rpm == self.last_rpms[-2] and rpm != 0:
				if self.same > 200 and not self.change:
					self.idle = rpm
					self.change = True
				self.same += 1
			else:
				self.same = 0
		except IndexError:
			pass
	
	def blink_rgb(self):
		"""
		This functions handels the blinking of the whole keyboard when a pitstop happens for example

		:param update:
		"""
		if self.blink:
			self.corsair.set_led_colors_async(led_colors=self.lights_on[0] + self.lights_on[1] + self.lights_on[2] + self.lights_on[3])
			self.blink = False
		else:
			self.corsair.set_led_colors_async(led_colors=self.lights_off[0] + self.lights_off[1] + self.lights_off[2] + self.lights_off[3])
			self.blink = True
	
	def handle_drs(self, drs_state: int):
		"""
		This functions handles the colors of the F5 to F8 buttons for the DRS

		:param drs_state:
		"""
		if drs_state >= 1:
			self.corsair.set_led_colors_async(led_colors=[
				CorsairLedColor(CLK.F5, 0, 0, 255),
				CorsairLedColor(CLK.F6, 0, 0, 255),
				CorsairLedColor(CLK.F7, 0, 0, 255),
				CorsairLedColor(CLK.F8, 0, 0, 255)]
				)
			if drs_state == 2:
				self.corsair.set_led_colors_async(led_colors=[
					CorsairLedColor(CLK.F5, 0, 255, 0),
					CorsairLedColor(CLK.F6, 0, 255, 0),
					CorsairLedColor(CLK.F7, 0, 255, 0),
					CorsairLedColor(CLK.F8, 0, 255, 0)]
					)
		else:
			self.corsair.set_led_colors_async(led_colors=[
				CorsairLedColor(CLK.F5, 0, 0, 0),
				CorsairLedColor(CLK.F6, 0, 0, 0),
				CorsairLedColor(CLK.F7, 0, 0, 0),
				CorsairLedColor(CLK.F8, 0, 0, 0)]
				)

	def handle_brakes(self, brake_state: float):
		"""
		This function handles the F9 to F12 buttons for the brake application

		:param brake_state:
		"""
		# setting up light states
		brake_lights_on = [
			CorsairLedColor(CLK.F12, 255, 0, 0),
			CorsairLedColor(CLK.F11, 255, 0, 0),
			CorsairLedColor(CLK.F10, 255, 0, 0),
			CorsairLedColor(CLK.F9, 255, 0, 0)
		]
		brake_lights_off = [
			CorsairLedColor(CLK.F12, 0, 0, 0),
			CorsairLedColor(CLK.F11, 0, 0, 0),
			CorsairLedColor(CLK.F10, 0, 0, 0),
			CorsairLedColor(CLK.F9, 0, 0, 0)
		]

		# setting up the index
		brake_index = round(brake_state * len(brake_lights_on))

		self.corsair.set_led_colors_async(led_colors=brake_lights_on[:brake_index] + brake_lights_off[brake_index:])			

	def handle_throttle(self, throttle_state: float):
		"""

		:param throttle_state:
		"""
		# Setting up ligth states
		gas_lights_on = [
			CorsairLedColor(CLK.F1, 0, 255, 0),
			CorsairLedColor(CLK.F2, 0, 255, 0), 
			CorsairLedColor(CLK.F3, 0, 255, 0),
			CorsairLedColor(CLK.F4, 0, 255, 0)
		]
		gas_lights_off = [
			CorsairLedColor(CLK.F1, 0, 0, 0),
			CorsairLedColor(CLK.F2, 0, 0, 0), 
			CorsairLedColor(CLK.F3, 0, 0, 0),
			CorsairLedColor(CLK.F4, 0, 0, 0)
		]

		# Setting up the index
		throttle_index = round(throttle_state * len(gas_lights_on))

		self.corsair.set_led_colors_async(led_colors=gas_lights_on[:throttle_index] + gas_lights_off[throttle_index:])

	def handle_battery(self, battery_state: float):
		"""

		:param battery_state:
		"""
		# Setting light states
		ligths_on = [
			[CorsairLedColor(CLK.Keypad0, 0, 255, 0),
			CorsairLedColor(CLK.KeypadPeriodAndDelete, 0, 255, 0)],
			[CorsairLedColor(CLK.Keypad1, 0, 255, 0),
			CorsairLedColor(CLK.Keypad2, 0, 255, 0),
			CorsairLedColor(CLK.Keypad3, 0, 255, 0)],
			[CorsairLedColor(CLK.Keypad4, 0, 255, 0),
			CorsairLedColor(CLK.Keypad5, 0, 255, 0),
			CorsairLedColor(CLK.Keypad6, 0, 255, 0)],
			[CorsairLedColor(CLK.Keypad7, 0, 255, 0),
			CorsairLedColor(CLK.Keypad8, 0, 255, 0),
			CorsairLedColor(CLK.Keypad9, 0, 255, 0)],
			[CorsairLedColor(CLK.NumLock, 0, 255, 0),
			CorsairLedColor(CLK.KeypadSlash, 0, 255, 0),
			CorsairLedColor(CLK.KeypadAsterisk, 0, 255, 0)]]
		lights_off = [
			[CorsairLedColor(CLK.Keypad0, 0, 0, 0),
			CorsairLedColor(CLK.KeypadPeriodAndDelete, 0, 0, 0)],
			[CorsairLedColor(CLK.Keypad1, 0, 0, 0),
			CorsairLedColor(CLK.Keypad2, 0, 0, 0),
			CorsairLedColor(CLK.Keypad3, 0, 0, 0)],
			[CorsairLedColor(CLK.Keypad4, 0, 0, 0),
			CorsairLedColor(CLK.Keypad5, 0, 0, 0),
			CorsairLedColor(CLK.Keypad6, 0, 0, 0)],
			[CorsairLedColor(CLK.Keypad7, 0, 0, 0),
			CorsairLedColor(CLK.Keypad8, 0, 0, 0),
			CorsairLedColor(CLK.Keypad9, 0, 0, 0)],
			[CorsairLedColor(CLK.NumLock, 0, 0, 0),
			CorsairLedColor(CLK.KeypadSlash, 0, 0, 0),
			CorsairLedColor(CLK.KeypadAsterisk, 0, 0, 0)]
		]
		battery_index = round(battery_state * 5)

		self.battery_index = battery_index

		lights = []

		for index, i in enumerate(ligths_on):
			if index < battery_index:
				lights += ligths_on[index]
			else:
				lights += lights_off[index]

		self.corsair.set_led_colors_async(led_colors=lights)

	def handle_deltas(self, delta: float):
		"""

		:param delta:
		"""
		delta = round(delta, 2)

		self.deltas.pop(0)
		self.deltas.append(delta)

		if self.deltas[0] > self.deltas[-1]:
			self.corsair.set_led_colors_async(led_colors=[
				CorsairLedColor(CLK.UpArrow, 0, 255, 0),
				CorsairLedColor(CLK.DownArrow, 0, 0, 0)
			])
		elif self.deltas[0] < self.deltas[-1]:
			self.corsair.set_led_colors_async(led_colors=[
				CorsairLedColor(CLK.UpArrow, 0, 0, 0),
				CorsairLedColor(CLK.DownArrow, 255, 0, 0)
			])
		else:
			self.corsair.set_led_colors_async(led_colors=[
				CorsairLedColor(CLK.UpArrow, 0, 0, 255),
				CorsairLedColor(CLK.DownArrow, 0, 0, 255)
			])

		if self.deltas[0] > 0:
			self.corsair.set_led_colors_async(
				led_colors=[
					CorsairLedColor(CLK.Insert, 0, 255, 0),
					CorsairLedColor(CLK.Home, 0, 255, 0),
					CorsairLedColor(CLK.PageDown, 0, 255, 0),
					CorsairLedColor(CLK.PageUp, 0, 255, 0),
					CorsairLedColor(CLK.End, 0, 255, 0),
					CorsairLedColor(CLK.Delete, 0, 255, 0)
				]
			)
		elif self.deltas[0] == 0:
			self.corsair.set_led_colors_async(
				led_colors=[
					CorsairLedColor(CLK.Insert, 0, 0, 255),
					CorsairLedColor(CLK.Home, 0, 0, 255),
					CorsairLedColor(CLK.PageDown, 0, 0, 255),
					CorsairLedColor(CLK.PageUp, 0, 0, 255),
					CorsairLedColor(CLK.End, 0, 0, 255),
					CorsairLedColor(CLK.Delete, 0, 0, 255)
				]
			)
		elif self.deltas[0] < 0:
			self.corsair.set_led_colors_async(
				led_colors=[
					CorsairLedColor(CLK.Insert, 255, 0, 0),
					CorsairLedColor(CLK.Home, 255, 0, 0),
					CorsairLedColor(CLK.PageDown, 255, 0, 0),
					CorsairLedColor(CLK.PageUp, 255, 0, 0),
					CorsairLedColor(CLK.End, 255, 0, 0),
					CorsairLedColor(CLK.Delete, 255, 0, 0)
				]
			)
		else:
			self.corsair.set_led_colors_async(
				led_colors=[
					CorsairLedColor(CLK.Insert, 0, 0, 0),
					CorsairLedColor(CLK.Home, 0, 0, 0),
					CorsairLedColor(CLK.PageDown, 0, 0, 0),
					CorsairLedColor(CLK.PageUp, 0, 0, 0),
					CorsairLedColor(CLK.End, 0, 0, 0),
					CorsairLedColor(CLK.Delete, 0, 0, 0)
				]
			)

	def handle_clutch(self, clutch_state: float):
		"""

		:param clutch_state:
		:return:
		"""

		ligths_on = [
			CorsairLedColor(CLK.F5, 0, 0, 255),
			CorsairLedColor(CLK.F8, 0, 0, 255),
			CorsairLedColor(CLK.F6, 0, 0, 255),
			CorsairLedColor(CLK.F7, 0, 0, 255)
		]
		ligths_off = [
			CorsairLedColor(CLK.F5, 0, 0, 0),
			CorsairLedColor(CLK.F8, 0, 0, 0),
			CorsairLedColor(CLK.F6, 0, 0, 0),
			CorsairLedColor(CLK.F7, 0, 0, 0)

		]

		percentage = round(((len(ligths_on) / 2) * percentage) * 2)
		self.corsair.set_led_colors_async(led_colors=ligths_on[:percentage] + ligths_off[percentage:])

	def set_max_rpm(self, max_rpm: int):
		self.max_rpm = max_rpm

	def set_mode(self, mode: str):
		self.mode = mode
		self.set_lights()

	def set_idle(self, idle: int):
		self.idle = idle






