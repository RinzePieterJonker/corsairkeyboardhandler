# Corsair keyboard handler for assetto corsa #

This project is a hobby project for me. One of my favorite things to do in my free time is to do some sim-racing in
Assetto Corsa because of its huge modding scene and because it is the closes that I am currently going to get to racing
in real life. So when I found out that I could use Python to link the lights of my keyboard to what my car does in 
Assetto Corsa, I immediately got plans in my head to make my keyboard respond to Assetto Corsa. This would make it
possible for me to combine 2 of my hobbies and learn some more from it.

## Installation ##
Installation can be done by cloning the repository to the apps/python folder in assetto corsa, the cloning can be done 
by using the following bash script
```bash
git clone https://RinzePieterJonker@bitbucket.org/RinzePieterJonker/corsairkeyboardhandler.git
```

## Prerequisites ##
For this app you would need a Corsair K70 Keyboard and Assetto Corsa, all the other prerequisites are already in the 
repository

## Usage ##
Usage guide is still work in progress