#!python3

"""

"""

# METADATA
__author__ = "Rinze-Pieter Jonker"
__version__ = "1.0"

# IMPORTS
import ac, acsys, configparser, os, json
from math import sin
from KeyBoardHandler import KeyBoardHandler

try:
	from cue_sdk import *
except ImportError as e:
	pass

# GLOBALS
config_file = "apps/python/corsairkeyboardhandler/resources/config.ini"
mode = "LeftToRight"

updates = 0
corsair_handler = 0

rpmLabel, idleLabel, maxLabel, minLabel, updatesLabel, batteryLabel, deltasLabel, testLabel = 0, 0, 0, 0, 0, 0, 0, 0
previewButton, cancelPreview, modeButton, centerswitcherButton, ersswitcherButton = 0, 0, 0, 0, 0
appWindow = 0
conf = 0
drs_clutch = "DRS"
ers_mode = "Battery"

testing = False
start_updates = 0

preview_speed = 100

# CODE
def acMain(ac_version):
	global corsair_handler, config_file, mode, conf, drs_clutch, ers_mode
	global appWindow
	global rpmLabel, idleLabel, maxLabel, minLabel, updatesLabel, batteryLabel, deltasLabel, testLabel
	global previewButton, cancelPreview, modeButton, centerswitcherButton, ersswitcherButton

	# Setting up debug app app
	appWindow = ac.newApp("Corsair Keyboard Debug")
	ac.setSize(appWindow, 500, 300)
	ac.setTitle(appWindow, "")

	# Loading in settings and calling the KeyBoardHandler class with the correct settings
	car_name = ac.getCarName(0)

	if os.path.exists(config_file):
		conf = configparser.ConfigParser()
		conf.read(config_file)
		if car_name in conf["mode"].keys():
			mode = conf["mode"][car_name]

		if car_name in conf["drs_clutch"].keys():
			drs_clutch = conf["drs_clutch"][car_name]

		if car_name in conf["ers_mode"].keys():
			ers_mode = conf["ers_mode"][car_name]

	# Loading the max rpm from the car
	car_json = "content/cars/{}/ui/ui_car.json".format(car_name)
	max_power = 0
	max_rpm = 0

	if os.path.exists(car_json):
		with open(car_json) as json_file:
			car_data = json.load(json_file)

			for step in car_data["powerCurve"]:
				if float(step[1]) >= max_power:
					max_power = float(step[1])
					max_rpm = float(step[0])

	corsair_handler = KeyBoardHandler(mode=mode, maximum=max_rpm)

	# Setting the text in the app
	rpmLabel = ac.addLabel(appWindow, "RPM: 0");
	ac.setPosition(rpmLabel, 3, 20)

	idleLabel = ac.addLabel(appWindow, "Idle Rpm: 0");
	ac.setPosition(idleLabel, 3, 40)

	maxLabel = ac.addLabel(appWindow, "Max Rpm: 0");
	ac.setPosition(maxLabel, 3, 60)

	minLabel = ac.addLabel(appWindow, "Min rpm: 0")
	ac.setPosition(minLabel, 3, 80)

	updatesLabel = ac.addLabel(appWindow, "Update Window: 0")
	ac.setPosition(updatesLabel, 3, 100)

	batteryLabel = ac.addLabel(appWindow, "Battery Index : ")
	ac.setPosition(batteryLabel, 3, 120)

	deltasLabel = ac.addLabel(appWindow, "Deltas: ")
	ac.setPosition(deltasLabel, 3, 140)

	testLabel = ac.addLabel(appWindow, "")
	ac.setPosition(testLabel, 3, 240)

	# setting the mode switching button
	modeLabel = ac.addLabel(appWindow, "Current mode: ")
	ac.setPosition(modeLabel, 3, 160)

	modeButton = ac.addButton(appWindow, "{}".format(corsair_handler.mode))
	ac.setPosition(modeButton,  163, 160)
	ac.setSize(modeButton, 120, 20)
	ac.addOnClickedListener(modeButton, switch_mode)

	# Setting up the preview start and cancel buttons
	previewLabel = ac.addLabel(appWindow, "Preview current mode:")
	ac.setPosition(previewLabel, 3, 180)

	previewButton = ac.addButton(appWindow, "Start Preview")
	ac.setPosition(previewButton, 163, 180)
	ac.setSize(previewButton, 120, 20)
	ac.addOnClickedListener(previewButton, preview)

	cancelPreview = ac.addButton(appWindow, "Cancel Preview")
	ac.setPosition(cancelPreview, 283, 180)
	ac.setSize(cancelPreview, 120, 20)
	ac.addOnClickedListener(cancelPreview, cancel_preview)

	# Setting up clutch/drs switcher
	centerswitcherLabel = ac.addLabel(appWindow, "Center ligths: ")
	ac.setPosition(centerswitcherLabel, 3, 200)

	centerswitcherButton = ac.addButton(appWindow, drs_clutch)
	ac.setPosition(centerswitcherButton, 163, 200)
	ac.setSize(centerswitcherButton, 120, 20)
	ac.addOnClickedListener(centerswitcherButton, switch_center)

	# Settings up battery mode (battery left / ers use left)
	ersswitcherLabel = ac.addLabel(appWindow, "ERS display mode: ")
	ac.setPosition(ersswitcherLabel, 3, 220)

	ersswitcherButton = ac.addButton(appWindow, ers_mode)
	ac.setPosition(ersswitcherButton, 163, 220)
	ac.setSize(ersswitcherButton, 120, 20)
	ac.addOnClickedListener(ersswitcherButton, switch_ers)

	# Setting up the save button
	saveSettings = ac.addButton(appWindow, "Save Settings")
	ac.setPosition(saveSettings, 375, 275)
	ac.setSize(saveSettings, 120, 20)
	ac.addOnClickedListener(saveSettings, save_settings)

	# try:
	# 	ac.setText(testLabel, " > trying to open json")
	# 	with codecs.open("content/cars/" + car_name + "/ui/ui_car.json", "r", "utf-8-sig") as uiFile:
	# 		uiDataString = uiFile.read().replace('\r', '').replace('\n', '').replace('\t', '')
	# 	uiDataJson = json.loads(uiDataString)
	# 	for step in uiDataJson["powerCurve"]:
	# 		if int(step[1]) >= maxPower:
	# 			maxPower = int(step[1])
	# 			maxPowerRpm = int(step[0])
	# 	ac.setText(testLabel, "loaded json")
	# except:
	# 	ac.setText(testLabel, "could not load json")
	return "Corsair Keyboard Lights"


def save_settings(*args):
	global appWindow, testLabel, config_file
	global mode, drs_clutch, ers_mode

	car_name = ac.getCarName(0)

	if os.path.exists(config_file):
		conf = configparser.ConfigParser()
		conf.read(config_file)
		conf["mode"][car_name] = mode
		conf["drs_clutch"][car_name] = drs_clutch
		conf["ers_mode"][car_name] = ers_mode
		with open(config_file, "w+") as config:
			conf.write(config)


def switch_mode(*args):
	global corsair_handler, modeButton, mode

	index = corsair_handler.modes.index(corsair_handler.mode)

	if index == len(corsair_handler.modes) - 1:
		next_index = 0
	else:
		next_index = index + 1

	new_mode = corsair_handler.modes[next_index]
	maximum, idle = corsair_handler.max_rpm, corsair_handler.idle
	ac.setText(modeButton, "{}".format(new_mode))
	corsair_handler = KeyBoardHandler(mode=new_mode, maximum=maximum, idle=idle)
	mode = new_mode


def switch_center(*args):
	global drs_clutch, centerswitcherButton

	if drs_clutch == "Clutch":
		drs_clutch = "DRS"
	elif drs_clutch == "DRS":
		drs_clutch = "Clutch"

	ac.setText(centerswitcherButton, drs_clutch)


def switch_ers(*args):
	global ers_mode, ersswitcherButton

	if ers_mode == "Battery":
		ers_mode = "Use"
	elif ers_mode == "Use":
		ers_mode = "Battery"

	ac.setText(ersswitcherButton, ers_mode)


def preview(*args):
	global appWindow, testLabel
	global testing, updates, start_updates

	testing = True
	start_updates = updates


def cancel_preview(*args):
	global appWindow, testLabel
	global testing, cancelPreview

	testing = False
	ac.setText(testLabel, "")


def retrieve_car_state():
	"""
	This function retrieves all the needed information form the Assetto corsa api and returns a dictionary with the
	information
	"""
	car_state = dict()

	car_state["rpm"] = round(ac.getCarState(0, acsys.CS.RPM))
	car_state["throttle"] = ac.getCarState(0, acsys.CS.Gas)
	car_state["brake"] = ac.getCarState(0, acsys.CS.Brake)
	car_state["battery"] = ac.getCarState(0, acsys.CS.KersCharge)
	car_state["battery_input"] = ac.getCarState(0, acsys.CS.KersInput)
	car_state["battery_lap_left"] = ac.getCarState(0, acsys.CS.ERSCurrentKJ)
	car_state["delta"] = ac.getCarState(0, acsys.CS.PerformanceMeter)
	car_state["clutch"] = 1 - ac.getCarState(0, acsys.CS.Clutch)

	drsAvailableValue = ac.getCarState(0, acsys.CS.DrsAvailable)
	drsEnabledValue = ac.getCarState(0, acsys.CS.DrsEnabled)

	if drsAvailableValue == 1 and drsEnabledValue == 1:
		car_state["DRS"] = 2
	elif drsAvailableValue == 1:
		car_state["DRS"] = 1
	else:
		car_state["DRS"] = 0
	return car_state


def acUpdate(deltaT):
	global updates, corsair_handler
	global rpmLabel, idleLabel, maxLabel, minLabel, updatesLabel, batteryLabel, deltasLabel, testLabel
	global testing, start_updates, preview_speed, drs_clutch

	car_state = retrieve_car_state()

	ac.setText(rpmLabel, "RPM: {}".format(car_state["rpm"]))
	ac.setText(idleLabel, "Idle rpm: {}".format(corsair_handler.idle))
	ac.setText(maxLabel, "Max rpm: {}".format(corsair_handler.max_rpm))
	ac.setText(minLabel, "Min rpm: {}".format(corsair_handler.min_rpm))
	ac.setText(updatesLabel, "Update Window : {}".format(updates))
	ac.setText(batteryLabel, "Battery Index: {}, {}".format(corsair_handler.battery_index, corsair_handler.battery_use_index))
	ac.setText(deltasLabel, "Deltas: {}, {}".format(corsair_handler.deltas[0],  corsair_handler.deltas[-1]))

	if testing:
		updates_after_start = updates - start_updates

		# Creating an alternating pattern for the percentages
		percentage = 0.5 + sin(updates_after_start / 4) * 0.5

		difference = round(corsair_handler.max_rpm - corsair_handler.min_rpm)
		rpm_state = round(difference * percentage + corsair_handler.min_rpm)

		ac.setText(testLabel, "{}".format(rpm_state))

		corsair_handler.update_rpm(rpm_state)
		corsair_handler.handle_brakes(percentage)
		corsair_handler.handle_throttle(percentage)
		if drs_clutch == "Clutch":
			corsair_handler.handle_clutch(percentage)
	else:
		corsair_handler.update_rpm(car_state["rpm"])

		if drs_clutch == "DRS":
			corsair_handler.handle_drs(car_state["DRS"])
		else:
			corsair_handler.handle_clutch(car_state["clutch"])

		corsair_handler.handle_brakes(car_state["brake"])
		corsair_handler.handle_throttle(car_state["throttle"])
		corsair_handler.handle_deltas(car_state["delta"])

		if updates % 20 == 0:
			corsair_handler.handle_battery(car_state["battery"])

	updates += 1
	pass